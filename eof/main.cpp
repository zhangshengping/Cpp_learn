#include <iostream>

using namespace std;

int main()
{
    int sum = 0, value;
    while (cin >> value)
    {
        sum += value; //equivalent to sum = sum + value
    }
    std::cout << "Sum is:" << sum << std::endl;
    return 0;
}
