#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define TRUE (1)

#define FALSE (0)

uint8_t get_string(char *string)
{
    char ch;
    char *p;
    p = string;

    while (ch = getchar())
    {
        if (32 != ch &&  9 != ch && 13 != ch && 10 != ch)
        {
            if (127 == ch)
            {
                if (p > string)
                {
                    p--;
                }
                else
                {
                    p = string;
                }
            }
            else
            {
                *(p++) = ch;
            }
        }
        else
        {
            break;
        }
    }

    *(p) = '\0';
    return TRUE;
}

uint8_t mifare1_data_get(char *string, uint8_t *p_data_length)
{
    char *p, *p_start;
    char i, flag;
    uint8_t value[2];
    p_start = p = string;
s0:

    while ('$' != *p && '\0' != *p)
    {

        if ('|' == *p)
        {
            while('$' != *p && '\0' != *p)
            {

                flag = 1;

                for (i = 0; i < 2; i++)
                {
                    p++;

                    if ('$' == *p || '\0' == *p)
                    {
                        return TRUE;
                    }

                    if (('0' <= *p) && ('9' >= *p))
                    {
                        value[i] = *p - '0';
                    }
                    else if (('a' <= *p) && ('f' >= *p))
                    {
                        value[i] = *p - 'a' + 10;;
                    }
                    else if (('A' <= *p) && ('F' >= *p))
                    {
                        value[i] = *p - 'A' + 10;;
                    }
                    else
                    {
                        string = p_start;
                        *p_data_length = 0;
                        goto s0;
                    }

                }
                (*p_data_length)++;
                *(string++) = value[0] * 16 + value[1];
            }
        }
        else
        {
            p++;
            goto s0;
        }
    }

    if (1 == flag)
    {
        return TRUE;
    }

    return FALSE;
}

int main()
{
    int i;
    char form_string[100];
    uint8_t length = 0;
    while(1)
    {
        length = 0x90;
        printf("0x%02x\n",length);
    }
    while (1)
    {
        printf(">");
        get_string(form_string);

        printf("\r\n");
        printf(form_string);
        printf("\r\n");
        mifare1_data_get(form_string, &length);
        printf("length %d\n", length);
        length = 0x90;
        printf("0x%02f\n",0x90);
        for (i = 0; i < length; i++)
        {
            printf("0x%02x ", form_string[i]);
        }
        printf("\r\n");
    }
    printf("Hello world!\n");
    return 0;
}
