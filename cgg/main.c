#include <stdio.h>
#include <stdlib.h>

int main()
{
    double inclk = 24000000, rate = 11059200;
    int i, j, div, mult, n;
    double err, herr, err_c, err_p;
    double fdiv;
    for (i = 2; i <= 256; i++)
    {
        fdiv = (double)inclk / rate;
        div = fdiv;

        mult = i * (fdiv - div);
        herr =  (inclk * i / ((i + mult) * div) - rate) / rate;
        err =  (rate - inclk * i / ((i + mult + 1) * div)) / rate;
        if (err < herr )
        {
            mult = mult + 1;
        }
        else
        {
            err = herr;
        }
        if (i == 2)
        {
            err_p = err;
        }
        if(err < err_p)
        {
            err_p = err;
        }
        printf("%d %d %d %f\r\n", mult, i, err_p, (float)err_p / rate);
    }
    printf("%d %d %d %f\r\n", mult, i, err_p, (float)err_p/rate);
}
