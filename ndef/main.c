#include <stdio.h>
#include <stdlib.h>

#include <stdint.h>

#define TRUE 1
#define FALSE 0
/******************************80字符参考线*************************************/
uint8_t test_memory[100][4];
uint8_t ntag_page_write (uint8_t page, uint8_t *p_data)
{
    int i;
    for(i = 0; i < 4; i++)
    {
        test_memory[page][i] = p_data[i];
    }
    return 1;
}

/* start */
uint8_t g_bt_format[12][4] = {
    {0x03, 0x34, 0xd2, 0x20},
    {0x11, 0x61, 0x70, 0x70},
    {0x6c, 0x69, 0x63, 0x61},
    {0x74, 0x69, 0x6f, 0x6e},
    {0x2f, 0x76, 0x6e, 0x64},
    {0x2e, 0x62, 0x6c, 0x75},
    {0x65, 0x74, 0x6f, 0x6f},
    {0x74, 0x68, 0x2e, 0x65},
    {0x70, 0x2e, 0x6f, 0x6f},
    {0x62, 0x11, 0x00, 0x40},
    {0x77, 0x67, 0x46, 0xf7},
    {0xac, 0x07, 0x09, 0x42},
};
const uint8_t g_bt_tlv_length_row = 0;   /* TLV L域在数组中位置的行号 */
const uint8_t g_bt_tlv_length_cow = 1;   /* TLV L域在数组中位置的列号 */

const uint8_t g_bt_payload_length_row = 1;
const uint8_t g_bt_payload_length_cow = 0;

const uint8_t g_bt_oob_length_row = 9;
const uint8_t g_bt_oob_length_cow = 1;

const uint8_t g_bt_eir_length_row = 11;
const uint8_t g_bt_eir_length_cow = 1;

const uint8_t g_bt_name_start_byte_row = 11;
const uint8_t g_bt_name_start_byte_cow = 3;

static uint8_t _calc_bt_tlv_length (uint8_t name_length)
{
    uint8_t length;
    length = name_length + (3 + 32 + 2 + 6 + 2);
    return length;
}

static uint8_t _calc_payload_length (uint8_t name_length)
{
    uint8_t length;
    length = name_length + (2 + 6 + 2);
    return length;
}
static uint16_t _calc_bt_oob_length (uint8_t name_length)
{
    uint16_t length;
    length = name_length + (2 + 6 + 2);
    return length;
}

static uint8_t _calc_bt_eir_length (uint8_t name_length)
{
    uint8_t length;
    length = name_length + 1;
    return length;
}

uint8_t set_tlv_length (uint8_t name_length)
{
    uint8_t length;
    length = _calc_bt_tlv_length(name_length);
    g_bt_format[g_bt_tlv_length_row][g_bt_tlv_length_cow] = length;

    if (length == g_bt_format[g_bt_tlv_length_row][g_bt_tlv_length_cow]) {
        return TRUE;
    } else {
        return FALSE;
    }
}

uint8_t set_payload_length (uint8_t name_length)
{
    uint8_t length;
    length = _calc_payload_length(name_length);
    g_bt_format[g_bt_payload_length_row][g_bt_payload_length_cow] = length;

    if (length == g_bt_format[g_bt_payload_length_row][g_bt_eir_length_cow]) {
        return TRUE;
    } else {
        return FALSE;
    }
}

uint8_t set_oob_length(uint8_t name_length)
{
    uint16_t length;
    length = _calc_bt_oob_length(name_length);
    g_bt_format[g_bt_oob_length_row][g_bt_oob_length_cow] = length & 0x0000ffff;
    g_bt_format[g_bt_oob_length_row][g_bt_oob_length_cow + 1] = length >> 8;

    if ((g_bt_format[g_bt_oob_length_row][g_bt_oob_length_cow] +
         g_bt_format[g_bt_oob_length_row][g_bt_oob_length_cow + 1] * 256)
        == length) {
        return TRUE;
    } else {
        return FALSE;
    }
}

uint8_t set_eir_length(uint8_t name_length)
{
    uint8_t length;
    length = _calc_bt_eir_length(name_length);
    g_bt_format[g_bt_eir_length_row][g_bt_eir_length_cow] = length;

    if (length ==  g_bt_format[g_bt_eir_length_row][g_bt_eir_length_cow]) {
        return TRUE;
    } else {
        return FALSE;
    }
}

uint8_t set_bt_address(uint8_t *addr)
{
    int i;
    uint8_t *address_start = &g_bt_format[9][3];

    for (i = 0; i < 6; i++) {
        *(address_start + 5 - i) = addr[i];
    }

    for (i = 0; i < 6; i++) {
        if (*(address_start + 5 - i) == addr[i]) {
            continue;
        } else {
            return FALSE;
        }
    }

    return TRUE;
}

uint8_t set_name_start_byte (char *name)
{
    g_bt_format[11][3] = name[0];
    return TRUE;
}

uint8_t set_name(char *name, uint8_t *name_length)
{
    return TRUE;
}


uint8_t set_format (const char *p_string, char *p_name, uint8_t *p_name_length)
{
    uint8_t bt_addr_string[12];
    uint8_t bt_addr[6];
    uint8_t i;
    uint8_t name_length;
    const char * const p_string_start = p_string;

S0:

    while (*p_string != '$')
    {
        if ('B' == *p_string)
        {
            p_string++;

            if ('T' == *p_string)
            {
                p_string++;

                if ('|' == *p_string)
                {
                    p_string++;
                    for (i = 0; i < 6; i++)
                    {


                        if ((*p_string >= '0' && *p_string <= '9') ||
                                (*p_string >= 'A' && *p_string <= 'F') ||
                                (*p_string >= 'a' && *p_string <= 'f'))
                        {
                            if (*p_string >= '0' && *p_string <= '9')
                            {
                                bt_addr_string[2 * i] = *p_string - '0';
                            }
                            else if (*p_string >= 'A' && *p_string <= 'F')
                            {
                                bt_addr_string[2 * i] = *p_string - 'A' + 10;
                            }
                            else
                            {
                                bt_addr_string[2 * i] = *p_string - 'a' + 10;
                            }

                            p_string++;
                        }
                        else
                        {
                            goto S0;
                        }

                        if ((*p_string >= '0' && *p_string <= '9') ||
                                (*p_string >= 'A' && *p_string <= 'F') ||
                                (*p_string >= 'a' && *p_string <= 'f'))
                        {
                            if (*p_string >= '0' && *p_string <= '9')
                            {
                                bt_addr_string[2 * i + 1] = *p_string - '0';
                            }
                            else if (*p_string >= 'A' && *p_string <= 'F')
                            {
                                bt_addr_string[2 * i + 1] = *p_string - 'A' + 10;
                            }
                            else
                            {
                                bt_addr_string[2 * i + 1] = *p_string - 'a' + 10;
                            }

                            p_string++;
                        }
                        else
                        {
                            goto S0;
                        }

                        if (':' == *p_string)
                        {
                            p_string++;
                        }
                        else
                        {
                            goto S0;
                        }
                    }

                    i = 0;
                    name_length = 0;

                    while (*p_string != '|')
                    {
                        name_length++;
                        p_name[i] = *p_string;
                        p_string++;
                        i++;
                    }

                    if (0 == name_length)
                    {
                        /* 无蓝牙名称处理 */

                    }

                    *p_name_length = name_length;
                    p_string++;

                    if ('T' == *p_string)
                    {
                        p_string++;

                        if ('B' == *p_string)
                        {
                            /* 计算地址 */
                            for (i = 0; i < 6; i++)
                            {
                                bt_addr[i] = bt_addr_string[2 * i] * 16 + bt_addr_string[2 * i + 1];
                            }

                            /* 数据写入缓冲 */
                            set_bt_address(bt_addr);
                            set_name_start_byte(p_name);
                            set_eir_length(name_length);
                            set_oob_length(name_length);
                            set_tlv_length(name_length);
                            set_payload_length(name_length);
                            p_string++;
                        }
                        else
                        {
                            goto S0;
                        }
                    }
                    else
                    {
                        goto S0;
                    }


                }
            }
            else
            {
                goto S0;
            }
        }
        else
        {
            p_string++;
            goto S0;
        }
    }
    if(100 >  p_string - p_string_start)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

uint8_t write_bt_oob_to_card(uint8_t start_page, char *p_name, uint8_t name_length)
{
    int page_num;
    uint8_t i = 0, j;
    uint8_t page_buff[4];
    uint8_t *p;
    uint8_t status;
    p = page_buff;

    for (i = 0; i < 12; i++) {
        status = ntag_page_write (start_page++, g_bt_format[i]);
        if(FALSE == status)
        {
            return FALSE;
        }
    }

    if (name_length % 4) {
        page_num = name_length / 4 ;
    } else {
        page_num = name_length / 4 - 1;
    }

    i = 0;

    for (; page_num >= 0; page_num--) {
        p = page_buff;

        if (0 == page_num) {
            for (j = 0; j < 4; j++) {
                if ( i < name_length - 1) {
                    *p = *(p_name + 1);
                    p_name++;
                    p++;
                    i++;
                } else {
                    *p = 0;
                    p++;
                    i++;
                }
            }
        } else {
            for (j = 0; j < 4; j++) {
                *p = *(p_name + 1);
                p_name++;
                p++;
                i++;
            }
        }

        status = ntag_page_write (start_page++, page_buff);
        if(FALSE == status)
        {
            return FALSE;
        }
    }

    return TRUE;
}


/* end   */


int main()
{
    uint8_t ch;
    char string[] = "www.baidu.com", form_string[] = "CDBT|AC:F7:46:67:77:40:BT-0212|TB$";
    set_format(form_string, string, &ch);
    write_bt_oob_to_card(4, string, ch);
    ch = 0;
    string[5] = ch;
    string[5] = '\0';
    printf(string);
//    printf("\a");
    getchar();
    return 0;
}
