#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>

const uint32_t leap_year[] = {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335};
const uint32_t non_leap_year[] = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};


void sec2tm(struct tm *p_time, uint32_t sec)
{
    uint32_t year, day, hour, min, i, j;
    uint32_t year_start_day[] =
    {
        0,
        366,
        366+365,
        366+365+365,
    };
    year = 4 *(sec / ((365*4 + 1)*24*60*60));
    p_time->tm_wday = ((sec/(24*60*60)) + 6) % 7;
    sec = sec % ((365*4 + 1)*24*60*60);
    day  = sec / (24 * 60 * 60);  // 4年中的第几天
    sec  = sec % (24 * 60 * 60);
    hour = sec / (60 * 60);
    sec  = sec % (60 * 60);
    min  = sec / 60;
    sec  = sec % 60;
    p_time->tm_sec = sec;
    p_time->tm_min = min;
    p_time->tm_hour = hour;
    i = 0;
    while (year_start_day[i] <= day )
    {
        i++;
    }
    i = i - 1;
    year += i;
    p_time->tm_year = year + 100;
    p_time->tm_yday = day - year_start_day[i];
    if (0 == i)
    {
        j = 0;
        while(p_time->tm_yday >= leap_year[j])
        {
            j++;
        }
        j = j-1;
        p_time->tm_mon = j;
        p_time->tm_mday = p_time->tm_yday - leap_year[j];
    }
    else
    {
        j = 0;
        while(p_time->tm_yday >= non_leap_year[j])
        {
            j++;
        }
        j = j-1;
        p_time->tm_mon = j;
        p_time->tm_mday = p_time->tm_yday - non_leap_year[j] + 1;
    }
    p_time->tm_isdst = 0;
}

uint32_t tm2sec(struct tm *p_time)
{
    uint32_t sec;
    sec = 0;
    sec += p_time->tm_sec;
    sec += p_time->tm_min * 60;
    sec += p_time->tm_hour * 60 * 60;
    sec += p_time->tm_yday * 24 * 60 * 60;
    sec += (p_time->tm_year - 100) * 365 *24 * 60 *60;
    sec += ((p_time->tm_year - 100) + 3) / 4 * 24 * 60 * 60;
    p_time->tm_isdst = 0;
    return sec;
}

int main()
{
    struct tm test;
    struct tm test_get;
    uint32_t secs = 24*60*60*366 +
                    24*60*60*31  +
                    24*60*60*28  +
                    24*60*60*31  +
                    24*60*60*8;
    uint32_t sec_get;

    sec2tm(&test, secs);

    sec_get = tm2sec(&test);

    sec2tm(&test_get, sec_get);

    printf("Hello world!\n");
    return 0;
}
